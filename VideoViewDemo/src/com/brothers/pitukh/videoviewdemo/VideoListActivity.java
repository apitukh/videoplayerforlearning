package com.brothers.pitukh.videoviewdemo;

import org.holoeverywhere.app.Activity;
import org.holoeverywhere.widget.GridView;
import org.holoeverywhere.widget.Toast;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ItemClick;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity(R.layout.video_grid)
public class VideoListActivity extends Activity {

	@ViewById(android.R.id.list)
	GridView grid;

	VideoListAdapter mVideoAdapter;
	VideoGridAnimator animator;

	static String TAG = "TAG";
	/* Constants used to switch from Grid to List and vice versa */
	// FIXME If you know a way to do this in pure XML please do it!
	private static final int GRID_ITEM_WIDTH_DP = 156;
	private static final int GRID_HORIZONTAL_SPACING_DP = 20;
	private static final int GRID_VERTICAL_SPACING_DP = 20;
	private static final int GRID_STRETCH_MODE = GridView.STRETCH_COLUMN_WIDTH;
	// private static final int GRID_STRETCH_MODE = GridView.STRETCH_SPACING_UNIFORM;
	private static final int LIST_HORIZONTAL_SPACING_DP = 0;
	private static final int LIST_VERTICAL_SPACING_DP = 10;
	private static final int LIST_STRETCH_MODE = GridView.STRETCH_COLUMN_WIDTH;

	static VideoListActivity act;

	@AfterViews
	public void after() {
		act = this;
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		mVideoAdapter = new VideoListAdapter(this);
		grid.setAdapter(mVideoAdapter);
		animator = new VideoGridAnimator(grid);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		animator.animate();
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		// Compute the left/right padding dynamically
		DisplayMetrics outMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
		int sidePadding = (int) (outMetrics.widthPixels / 100 * Math.pow(outMetrics.density, 3) / 2);
		sidePadding = Math.max(0, Math.min(100, sidePadding));
		grid.setPadding(sidePadding, grid.getPaddingTop(), sidePadding, grid.getPaddingBottom());

		// Select between grid or list
		if (hasSpaceForGrid(grid)) { // grid?????????????????????
			Log.d(TAG, "Switching to grid mode");
			grid.setNumColumns(GridView.AUTO_FIT);
			grid.setStretchMode(GRID_STRETCH_MODE);
			grid.setHorizontalSpacing(Util.convertDpToPx(getApplication(), GRID_HORIZONTAL_SPACING_DP));
			grid.setVerticalSpacing(Util.convertDpToPx(getApplication(), GRID_VERTICAL_SPACING_DP));
			grid.setColumnWidth(Util.convertDpToPx(getApplication(), GRID_ITEM_WIDTH_DP));
			mVideoAdapter.setListMode(false);
		} else {
			Log.d(TAG, "Switching to list mode");
			grid.setNumColumns(1);
			grid.setStretchMode(LIST_STRETCH_MODE);
			grid.setHorizontalSpacing(LIST_HORIZONTAL_SPACING_DP);
			grid.setVerticalSpacing(Util.convertDpToPx(getApplication(), LIST_VERTICAL_SPACING_DP));
			mVideoAdapter.setListMode(true);
		}
	}

	private boolean hasSpaceForGrid(View v) {
		DisplayMetrics outMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(outMetrics);

		final int itemWidth = Util.convertDpToPx(getApplication(), GRID_ITEM_WIDTH_DP);
		final int horizontalspacing = Util.convertDpToPx(getApplication(), GRID_HORIZONTAL_SPACING_DP);
		final int width = grid.getPaddingLeft() + grid.getPaddingRight() + horizontalspacing + (itemWidth * 2);
		if (width < outMetrics.widthPixels)
			return true;
		return false;
	}

	@ItemClick(android.R.id.list)
	public void listItemClicked(Media clickedItem) {
		Intent i = new Intent(this, VideoPlayerActivity_.class);
		i.putExtra(VideoPlayerActivity.EXTRA_VIDEOPLAYER_FILE_PATH, clickedItem.getPath());
		i.putExtra(VideoPlayerActivity.EXTRA_VIDEOPLAYER_START, true);
		startActivity(i);
	}

	boolean openSavedPhrasesClicked(Media fileInfo) {
		String videoFilePath = fileInfo.getPath();
		SharedPreferences settings = getSharedPreferences(ConstantsShared.SHARED_VIDEO_TO_SUBS, Context.MODE_PRIVATE);
		String subtitleFilePath = settings.getString(videoFilePath, null);
		if (subtitleFilePath == null) {
			String noBoundSubtitleFile = getString(R.string.videos_no_bound_subtitles);
			Toast.makeText(this, noBoundSubtitleFile, Toast.LENGTH_LONG).show();
			return true;
		}

		Intent intent = new Intent(this, SavedPhrasesActivity_.class);
		intent.putExtra(SavedPhrasesActivity.EXTRA_VIDEO_FILE, videoFilePath);
		intent.putExtra(SavedPhrasesActivity.EXTRA_SUBTITLES_FILE, subtitleFilePath);
		act.startActivity(intent);
		return true;
	}

	public boolean readSubtitlesClicked(Media fileInformation) {
		return true;
	}

}
