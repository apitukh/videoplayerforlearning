package com.brothers.pitukh.videoviewdemo.subtitles;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Subtitles {

	public static class SubItem {

		private Date startDate;
		private Date endDate;
		private String text;

		public SubItem(long startTime, long endTime, String lineString) {
			startDate = new Date(startTime);
			endDate = new Date(endTime);
			text = lineString;
		}

		public Date getStartDate() {
			return startDate;
		}

		public Date getEndDate() {
			return endDate;
		}

		public String getText() {
			return text;
		}

	}

	List<SubItem> data = new ArrayList<SubItem>();

	void putSubItem(SubItem item) {
		data.add(item);
	}

	public SubItem getItemAt(int position) {
		if (position >= data.size()) {
			return null;
		}
		return data.get(position);
	}


	public static Subtitles from(String subtitleFile) throws IOException {
		return Subtitles.from(new File(subtitleFile));
	}

	public static Subtitles from(File subsPath) throws IOException {
		LineNumberReader r = new LineNumberReader(new FileReader(subsPath));
		Subtitles track = new Subtitles();
		String numberString;
		while ((numberString = r.readLine()) != null) {
			if (numberString.isEmpty()) {
				continue;
			}
			String timeString = r.readLine();
			String lineString = "";
			String s;
			while (!((s = r.readLine()) == null || s.trim().equals(""))) {
				lineString += s + "\n";
			}

			long startTime = parse(timeString.split("-->")[0]);
			long endTime = parse(timeString.split("-->")[1]);

			track.putSubItem(new SubItem(startTime, endTime, lineString.trim()));
		}
		return track;
	}

	private static long parse(String in) {
		long hours = Long.parseLong(in.split(":")[0].trim());
		long minutes = Long.parseLong(in.split(":")[1].trim());
		long seconds = Long.parseLong(in.split(":")[2].split(",")[0].trim());
		long millies = Long.parseLong(in.split(":")[2].split(",")[1].trim());

		return hours * 60 * 60 * 1000 + minutes * 60 * 1000 + seconds * 1000 + millies;

	}

	public int getItemAtTime(long ms) {
		int i = 0;
		for (SubItem item : data) {
			if (ms <= item.getEndDate().getTime()) {
				return i;
			}
			i++;
		}
		throw new IllegalArgumentException("This time is above existing ranges of subtitles");
	}

	public int getSize() {
		return data.size();
	}

}
