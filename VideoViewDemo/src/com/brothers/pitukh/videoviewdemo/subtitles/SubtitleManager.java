package com.brothers.pitukh.videoviewdemo.subtitles;

import java.io.File;
import java.io.IOException;

import android.util.Log;
import android.widget.VideoView;

import com.brothers.pitukh.videoviewdemo.Ap;
import com.brothers.pitukh.videoviewdemo.subtitles.Subtitles.SubItem;
import com.googlecode.androidannotations.api.BackgroundExecutor;

public class SubtitleManager {

	public interface ISubtitleListener {
		public void subtitleFinished();

		public void subtitleStarted(SubItem newItem);
	}

	private File subtitleFile;
	private ISubtitleListener listener;
	private Subtitles subs;
	private VideoView videoView;
	private Thread backgroundThread;
	private int curSubtitleNumber = 0;

	@SuppressWarnings("unused")
	private SubtitleManager() throws Exception {
		throw new Exception("This must not be called");
	}

	public SubtitleManager(File subtitleFile, ISubtitleListener listener, VideoView videoView) throws IOException {
		this.subtitleFile = subtitleFile;
		this.listener = listener;
		this.subs = Subtitles.from(subtitleFile);
		this.videoView = videoView;
	}

	// TODO: return empty item if there is time between 2 subtitles
	public SubItem getCurrentSubtitle() {
		return this.subs.getItemAt(this.curSubtitleNumber);
	}

	public int getCurrentSubtitleNumber() {
		return this.curSubtitleNumber;
	}

	public File getSubtitleFile() {
		return subtitleFile;
	}

	public Subtitles getSubtitles() {
		return this.subs;
	}

	public void setSubsFile(File subtitleFile) {
		this.subtitleFile = subtitleFile;
		try {
			this.subs = Subtitles.from(subtitleFile);
		} catch (Exception e) {
			Log.e(Ap.TAG, "Subtitles.from(subtitleFile); failed", e);
		}
	}

	public void setSubsFile(String subtitleAbsoluteFilePath) {
		this.subtitleFile = new File(subtitleAbsoluteFilePath);
	}

	public void start(final long ms) {
		if (backgroundThread != null) {
			stop();
		}
		BackgroundExecutor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					startSubsWorker(ms);
				} catch (RuntimeException e) {
					Log.e(Ap.TAG, "A runtime exception was thrown while executing code in a runnable", e);
				}
			}
		});
	}

	private void startSubsWorker(long ms) {
		if (this.backgroundThread == null) {
			this.backgroundThread = Thread.currentThread();
		} else {
			Log.e(Ap.TAG, "THIS SITUATION MUST NOT EXIST");
			return;
		}
		int tempCurSubtitleNumber = subs.getItemAtTime(ms);
		// prevent subtitle "blinks"
		if (tempCurSubtitleNumber != curSubtitleNumber) {
			listener.subtitleFinished();
		}
		curSubtitleNumber = tempCurSubtitleNumber;
		SubItem curSubtitleItem = subs.getItemAt(curSubtitleNumber);
		try {
			while (curSubtitleItem != null) {
				long timeToSleep = curSubtitleItem.getStartDate().getTime() - videoView.getCurrentPosition();
				if (timeToSleep > 0) {
					Thread.sleep(timeToSleep);
				}
				listener.subtitleStarted(curSubtitleItem);
				long diff = curSubtitleItem.getEndDate().getTime() - videoView.getCurrentPosition();
				if (diff < 0) {
					curSubtitleNumber = subs.getItemAtTime(videoView.getCurrentPosition());
					curSubtitleItem = subs.getItemAt(curSubtitleNumber);
					continue;
				}
				Thread.sleep(diff);
				listener.subtitleFinished();
				curSubtitleNumber++;
				curSubtitleItem = subs.getItemAt(curSubtitleNumber);
			}
		} catch (InterruptedException e) {
			Log.v(Ap.TAG, "SubtitleManager's thread was interrupted");
		}
	}

	public void stop() {
		if (this.backgroundThread != null) {
			this.backgroundThread.interrupt();
			this.backgroundThread = null;
		}
	}

}