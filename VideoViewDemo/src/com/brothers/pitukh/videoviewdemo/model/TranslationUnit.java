package com.brothers.pitukh.videoviewdemo.model;

public class TranslationUnit {

	private int subtitleNumber;
	private int startIndex;
	private int endIndex;
	private String translation;

	public TranslationUnit(int subtitleNumber, int startIndex, int endIndex, String translation) {
		this.subtitleNumber = subtitleNumber;
		this.startIndex = startIndex;
		this.endIndex = endIndex;
		this.translation = translation;
	}

	public TranslationUnit() {
		// TODO Auto-generated constructor stub
	}

	public int getSubtitleNumber() {
		return subtitleNumber;
	}

	public void setSubtitleNumber(int subtitleNumber) {
		this.subtitleNumber = subtitleNumber;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	public int getEndIndex() {
		return endIndex;
	}

	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}

	public String getTranslation() {
		return translation;
	}

	public void setTranslation(String translation) {
		this.translation = translation;
	}

}
