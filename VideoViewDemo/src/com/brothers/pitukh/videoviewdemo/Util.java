package com.brothers.pitukh.videoviewdemo;

import static com.nineoldandroids.view.animation.AnimatorProxy.NEEDS_PROXY;
import static com.nineoldandroids.view.animation.AnimatorProxy.wrap;

import java.io.File;
import java.util.Formatter;
import java.util.Locale;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.content.CursorLoader;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

public class Util {

	public static class WordBounds {
		public int startIndex;
		public int endIndex;
	}

	public static WordBounds getWordBoundsFromText(String text, int position) {
		WordBounds result = new WordBounds();
		int startPos = -1;
		int endPos = -1;

		text = text.replaceAll("\r|\n", " ");

		if (position >= text.length()) {
			position = text.length() - 1;
		}
		if (position < 0) {
			position = 0;
		}

		// Search for start position
		if (text.charAt(position) == ' ') {
			for (int i = position + 1; i < text.length(); i++) {
				if (text.charAt(i) != ' ') {
					startPos = i;
					break;
				}
			}
		} else {
			for (int i = position - 1; i >= 0; i--) {
				if (text.charAt(i) == ' ') {
					startPos = i + 1;
					break;
				}
			}
		}

		if (startPos == -1) {
			startPos = 0;
		}

		// Search for end position from the found start position
		for (int i = startPos + 1; i < text.length(); i++) {
			if (text.charAt(i) == ' ') {
				endPos = i - 1;
				break;
			}
		}

		if (endPos == -1) {
			endPos = text.length() - 1;
		}

		result.startIndex = startPos;
		result.endIndex = endPos;

		return result;
	}

	/***
	 * Get the character offset closest to the specified absolute position. A typical use case is to pass the result of getX() and getY() to
	 * this method.
	 */
	public static int getOffsetForPosition(TextView textView, float x, float y) {
		if (textView.getLayout() == null) {
			return -1;
		}
		final int line = getLineAtCoordinate(textView, y);
		final int offset = getOffsetAtCoordinate(textView, line, x);
		return offset;
	}

	public static int getOffsetAtCoordinate(TextView textView2, int line, float x) {
		if (textView2.getLayout() == null) {
			return -1;
		}
		x = convertToLocalHorizontalCoordinate(textView2, x);
		return textView2.getLayout().getOffsetForHorizontal(line, x);
	}

	private static float convertToLocalHorizontalCoordinate(TextView textView2, float x) {
		x -= textView2.getTotalPaddingLeft();
		// Clamp the position to inside of the view.
		x = Math.max(0.0f, x);
		x = Math.min(textView2.getWidth() - textView2.getTotalPaddingRight() - 1, x);
		x += textView2.getScrollX();
		return x;
	}

	private static int getLineAtCoordinate(TextView textView2, float y) {
		y -= textView2.getTotalPaddingTop();
		// Clamp the position to inside of the view.
		y = Math.max(0.0f, y);
		y = Math.min(textView2.getHeight() - textView2.getTotalPaddingBottom() - 1, y);
		y += textView2.getScrollY();
		return textView2.getLayout().getLineForVertical((int) y);
	}

	// Source from grepCode (android.widget.MediaController)
	public static String stringForTime(int timeMs) {
		int totalSeconds = timeMs / 1000;

		int seconds = totalSeconds % 60;
		int minutes = (totalSeconds / 60) % 60;
		int hours = totalSeconds / 3600;

		StringBuilder mFormatBuilder = new StringBuilder();
		Formatter mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
		mFormatBuilder.setLength(0);
		if (hours > 0) {
			return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
		} else {
			return mFormatter.format("%02d:%02d", minutes, seconds).toString();
		}
	}

	public static void openWordInGoogleTranslate(Context context, String word) {
		// Open selected word in Google Translate application
		if (isAppInstalled(context, "com.google.android.apps.translate")) {
			Intent i = context.getPackageManager().getLaunchIntentForPackage("com.google.android.apps.translate");
			i.setAction(Intent.ACTION_VIEW);
			i.putExtra("key_text_input", word);
			i.putExtra("key_text_output", "");
			i.putExtra("key_language_from", "en");
			i.putExtra("key_language_to", "uk");
			i.putExtra("key_suggest_translation", "");
			i.putExtra("key_from_floating_window", false);
			context.startActivity(i);
		} else {
			String errorMsg = context.getString(R.string.google_translate_not_installed);
			Toast.makeText(context, errorMsg, Toast.LENGTH_SHORT).show();
		}
	}

	private static boolean isAppInstalled(Context context, String uri) {
		PackageManager pm = context.getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	public static void checkOnMainThread() {
		if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
			throw new IllegalStateException("This method should be called from the Main Thread");
		}
	}

	public static AlertDialog getDialog() {
		return null;
	}

	public static void setAlpha(View view, float alpha) {
		if (NEEDS_PROXY) {
			wrap(view).setAlpha(alpha);
		} else {
			view.setAlpha(alpha);
		}
	}

	public static long getVideoDuration(Context context, String path) {
		return getVideoDuration(context, new File(path));
	}

	public static long getVideoDuration(Context context, File file) {
		String[] projection = { MediaStore.Video.Media.DURATION };
		String selection = MediaStore.Video.Media.DATA + "=?";
		String[] selectionArgs = { file.getAbsolutePath() };
		Cursor cursor = new CursorLoader(context, MediaStore.Video.Media.EXTERNAL_CONTENT_URI, projection, selection, selectionArgs, null)
				.loadInBackground();
		while (cursor != null && cursor.moveToFirst()) {
			return cursor.getLong(0);
		}
		return 0;
	}

	public static int convertPxToDp(Application app, int px) {
		WindowManager wm = (WindowManager) app.getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);
		float logicalDensity = metrics.density;
		int dp = Math.round(px / logicalDensity);
		return dp;
	}

	public static int convertDpToPx(Application app, int dp) {
		return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, app.getResources().getDisplayMetrics()));
	}

}
