package com.brothers.pitukh.videoviewdemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.holoeverywhere.widget.PopupMenu;
import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Thumbnails;
import android.support.v4.content.CursorLoader;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EBean;
import com.googlecode.androidannotations.annotations.UiThread;

@EBean
public class VideoListAdapter extends BaseAdapter {

	private final VideoListActivity context;
	private List<Media> info = new ArrayList<Media>();
	BitmapCache bitmapCache;

	public VideoListAdapter(Context context) {
		this.context = (VideoListActivity) context;
		bitmapCache = BitmapCache.getInstance();
		MediaDatabase mediaDB = MediaDatabase.getInstance(context);
		HashMap<String, Media> medias = mediaDB.getMedias();
		if (medias.isEmpty()) {
			Cursor cursor = new CursorLoader(context, MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, null, null, null)
					.loadInBackground();
			while (cursor.moveToNext()) {
				long length = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
				String title = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME));
				if (!title.endsWith(".mp4")) {
					continue;
				}
				String location = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
				int width = 0, height = 0;
				if (Integer.valueOf(android.os.Build.VERSION.SDK) >= 16) {
					width = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.Media.WIDTH));
					height = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.Media.HEIGHT));
				}
				// int type = cursor.getInt(cursor.getColumnIndex(MediaStore.Video.Media.WIDTH));
				int type = 0;
				long time = 0;

				Media media = new Media(location, time, length, type, null, title, width, height);
				mediaDB.addMedia(media);
				info.add(media);
			}
		} else {
			info.addAll(medias.values());
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View resultView = null;
		if (convertView != null) {
			resultView = convertView;
		} else {
			if (isListMode) {
				resultView = View.inflate(context, R.layout.video_list_item, null);
			} else {
				resultView = View.inflate(context, R.layout.video_grid_item, null);
			}
		}
		TextView videoFileName = ((TextView) resultView.findViewById(R.id.ml_item_title));
		ImageView videoThumbnail = ((ImageView) resultView.findViewById(R.id.ml_item_thumbnail));
		TextView videoDuration = ((TextView) resultView.findViewById(R.id.ml_item_subtitle));
		ImageView moreButton = ((ImageView) resultView.findViewById(R.id.ml_item_more));

		final Media fileInformation = getItem(position);
		videoFileName.setText(fileInformation.getTitle());
		videoDuration.setText(Util.stringForTime((int) fileInformation.getDuration()));
		Bitmap cachedBitmap = bitmapCache.getBitmapFromMemCache(fileInformation.getPath());
		if (cachedBitmap == null) {
			processBitmapInBackground(videoThumbnail, fileInformation);
		} else {
			videoThumbnail.setImageBitmap(cachedBitmap);
		}
		moreButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				PopupMenu popupMenu = new PopupMenu(context, view);
				Menu menu = popupMenu.getMenu();
				MenuItem item1 = menu.add(Menu.NONE, 1, Menu.NONE, context.getString(R.string.videos_saved_phrases));
				MenuItem item2 = menu.add(Menu.NONE, 2, Menu.NONE, "Read subtitles");
				item1.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						return context.openSavedPhrasesClicked(fileInformation);
					}
				});
				item2.setOnMenuItemClickListener(new OnMenuItemClickListener() {
					@Override
					public boolean onMenuItemClick(MenuItem item) {
						return context.readSubtitlesClicked(fileInformation);
					}
				});
				popupMenu.show();
			}
		});

		return resultView;
	}

	@Background
	void processBitmapInBackground(ImageView videoThumbnail, Media media) {
		Bitmap newCachedValue = ThumbnailUtils.createVideoThumbnail(media.getPath(), Thumbnails.MINI_KIND);
		bitmapCache.addBitmapToMemCache(media.getPath(), newCachedValue);
		setImageBitmap(videoThumbnail, newCachedValue);
	}

	@UiThread
	void setImageBitmap(ImageView videoThumbnail, Bitmap newCachedValue) {
		videoThumbnail.setImageBitmap(newCachedValue);
	}

	@Override
	public int getCount() {
		return info.size();
	}

	@Override
	public Media getItem(int position) {
		return info.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	private boolean isListMode = true;

	public void setListMode(boolean b) {
		isListMode = b;
	}

	public boolean isListMode() {
		return isListMode;
	}

}
