package com.brothers.pitukh.videoviewdemo;

public class Media {

	private String path;
	private long time;
	private long duration;
	private int type;
	private Object picture;
	private String title;
	private int width;
	private int height;

	public Media(String path, long time, long duration, int type, Object picture, String title, int width, int height) {
		super();
		this.path = path;
		this.time = time;
		this.duration = duration;
		this.type = type;
		this.picture = picture;
		this.title = title;
		this.width = width;
		this.height = height;
	}

	public String getPath() {
		return path;
	}

	public long getTime() {
		return time;
	}

	public long getDuration() {
		return duration;
	}

	public int getType() {
		return type;
	}

	public Object getPicture() {
		return picture;
	}

	public String getTitle() {
		return title;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public String getDisplayName() {
		if (title.contains(".mp4")) {
			return title.substring(title.indexOf(".mp4") + 1);
		}
		return title;
	}

}
