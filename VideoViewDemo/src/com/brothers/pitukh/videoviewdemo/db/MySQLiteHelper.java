package com.brothers.pitukh.videoviewdemo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.brothers.pitukh.videoviewdemo.Ap;

/***
 * This class is responsible for database creation and upgrading (that is, dropping and recreation).
 * 
 * @author APitukh
 * 
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_TRANSLATIONS = "translation";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_FILE_PATH = "file_path";
	public static final String COLUMN_SUBTITLE_NUMBER = "subtitle_number";
	public static final String COLUMN_START_INDEX = "start_index";
	public static final String COLUMN_END_INDEX = "end_index";
	public static final String COLUMN_TRANSLATION = "translation";

	private static final String DATABASE_NAME = "language_learning.db";
	private static final int DATABASE_VERSION = 2;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table " + TABLE_TRANSLATIONS + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_FILE_PATH + " text not null, " + COLUMN_SUBTITLE_NUMBER
			+ " integer not null, " + COLUMN_START_INDEX + " integer not null, " + COLUMN_END_INDEX + " integer not null, "
			+ COLUMN_TRANSLATION + " text, UNIQUE (" + COLUMN_FILE_PATH + "," + COLUMN_SUBTITLE_NUMBER + "," + COLUMN_START_INDEX + ","
			+ COLUMN_END_INDEX + "));";

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(Ap.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRANSLATIONS);
		onCreate(db);
	}

}