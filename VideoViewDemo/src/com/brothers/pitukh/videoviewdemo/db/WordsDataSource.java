package com.brothers.pitukh.videoviewdemo.db;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brothers.pitukh.videoviewdemo.Ap;
import com.brothers.pitukh.videoviewdemo.model.TranslationUnit;

/***
 * Provide convenient interface for interaction with Database (MySQLiteHelper): add, get and delete (TODO: we have to delete articles after
 * some time
 * has passed) articles from database
 * 
 * @author APitukh
 * 
 */
public class WordsDataSource {

	// Database fields
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = { MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_FILE_PATH, MySQLiteHelper.COLUMN_SUBTITLE_NUMBER,
			MySQLiteHelper.COLUMN_START_INDEX, MySQLiteHelper.COLUMN_END_INDEX, MySQLiteHelper.COLUMN_TRANSLATION };

	public WordsDataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	private void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	private boolean isOpened() {
		if (database == null) {
			return false;
		}
		return database.isOpen();
	}

	private void close() {
		dbHelper.close();
	}

	public boolean addTranslation(File subtitleFile, TranslationUnit item) {
		return addTranslation(subtitleFile.getAbsolutePath(), item);
	}

	public boolean addTranslation(String filePath, TranslationUnit newUnit) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_FILE_PATH, filePath);
		values.put(MySQLiteHelper.COLUMN_SUBTITLE_NUMBER, newUnit.getSubtitleNumber());
		values.put(MySQLiteHelper.COLUMN_START_INDEX, newUnit.getStartIndex());
		values.put(MySQLiteHelper.COLUMN_END_INDEX, newUnit.getEndIndex());
		values.put(MySQLiteHelper.COLUMN_TRANSLATION, newUnit.getTranslation());
		try {
			if (!isOpened()) {
				open();
			}
			long result = database.insertOrThrow(MySQLiteHelper.TABLE_TRANSLATIONS, null, values);
			if (result == -1) {
				Log.e(Ap.TAG, "Some error occured when inserting into database");
				return false;
			}
		} catch (SQLiteConstraintException e) {
			Log.v(Ap.TAG, "Translation with existing parameters failed to be written to the database", e);
			return false;
		} finally {
			close();
		}
		return true;
	}

	public List<TranslationUnit> getTranslations(String filePath, int subtitleNumber) {
		List<TranslationUnit> result = new ArrayList<TranslationUnit>();
		if (!isOpened()) {
			open();
		}
		Cursor cursor = database.query(MySQLiteHelper.TABLE_TRANSLATIONS, allColumns, MySQLiteHelper.COLUMN_FILE_PATH + "=? AND "
				+ MySQLiteHelper.COLUMN_SUBTITLE_NUMBER + "=?", new String[] { filePath, "" + subtitleNumber }, null, null, null);
		while (cursor.moveToNext()) {
			result.add(cursorToTranslationUnit(cursor));
		}
		close();
		return result;
	}

	private TranslationUnit cursorToTranslationUnit(Cursor cursor) {
		TranslationUnit result = new TranslationUnit();
		result.setSubtitleNumber(cursor.getInt(2));
		result.setStartIndex(cursor.getInt(3));
		result.setEndIndex(cursor.getInt(4));
		result.setTranslation(cursor.getString(5));
		return result;
	}

	public boolean updateTranslation(File filePath, TranslationUnit item) {
		return updateTranslation(filePath.getAbsolutePath(), item);
	}

	public boolean updateTranslation(String filePath, TranslationUnit item) {
		String whereClause = MySQLiteHelper.COLUMN_FILE_PATH + "=? AND " + MySQLiteHelper.COLUMN_SUBTITLE_NUMBER + "=? AND "
				+ MySQLiteHelper.COLUMN_START_INDEX + "=? AND " + MySQLiteHelper.COLUMN_END_INDEX + "=?";
		String[] whereArgs = new String[] { filePath, "" + item.getSubtitleNumber(), "" + item.getStartIndex(), "" + item.getEndIndex() };
		ContentValues args = new ContentValues();
		args.put(MySQLiteHelper.COLUMN_TRANSLATION, item.getTranslation());
		if (!isOpened()) {
			open();
		}
		int affectedRowsNum = this.database.update(MySQLiteHelper.TABLE_TRANSLATIONS, args, whereClause, whereArgs);
		close();
		if (affectedRowsNum != 1) {
			Log.e(Ap.TAG, "The number of affected rows is not one: " + affectedRowsNum);
			// TODO: do we need to return false here?
			return false;
		}
		return true;
	}

	public boolean remove(File file, TranslationUnit item) {
		return remove(file.getAbsolutePath(), item);
	}

	public boolean remove(String filePath, TranslationUnit item) {
		String whereClause = MySQLiteHelper.COLUMN_FILE_PATH + "=? AND " + MySQLiteHelper.COLUMN_SUBTITLE_NUMBER + "=? AND "
				+ MySQLiteHelper.COLUMN_START_INDEX + "=? AND " + MySQLiteHelper.COLUMN_END_INDEX + "=?";
		String[] whereArgs = new String[] { filePath, "" + item.getSubtitleNumber(), "" + item.getStartIndex(), "" + item.getEndIndex() };
		if (!isOpened()) {
			open();
		}
		int affectedRows = database.delete(MySQLiteHelper.TABLE_TRANSLATIONS, whereClause, whereArgs);
		close();
		if (affectedRows == 0) {
			return false;
		} else {
			return true;
		}
	}

	public List<TranslationUnit> getAllTranslations(String filePath) {
		List<TranslationUnit> result = new ArrayList<TranslationUnit>();
		if (!isOpened()) {
			open();
		}
		Cursor cursor = database.query(MySQLiteHelper.TABLE_TRANSLATIONS, allColumns, MySQLiteHelper.COLUMN_FILE_PATH + "=?",
				new String[] { filePath }, null, null, null);
		while (cursor.moveToNext()) {
			result.add(cursorToTranslationUnit(cursor));
		}
		close();
		return result;
	}

	public List<TranslationUnit> getAllTranslations(File subsPath) {
		return getAllTranslations(subsPath.getAbsolutePath());
	}

}