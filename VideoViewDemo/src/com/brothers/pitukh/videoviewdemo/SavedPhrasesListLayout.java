package com.brothers.pitukh.videoviewdemo;

import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.TextView;

import android.content.Context;
import android.text.Html;

import com.brothers.pitukh.videoviewdemo.model.TranslationUnit;
import com.brothers.pitukh.videoviewdemo.subtitles.Subtitles.SubItem;
import com.googlecode.androidannotations.annotations.App;
import com.googlecode.androidannotations.annotations.EViewGroup;
import com.googlecode.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.item_saved_phrase)
public class SavedPhrasesListLayout extends LinearLayout {

	@ViewById(R.id.startTime)
	protected TextView startTime;
	@ViewById(R.id.originalText)
	protected TextView originalText;
	@ViewById(R.id.context)
	protected TextView context;
	@ViewById(R.id.translatedText)
	protected TextView translatedText;
	@App
	protected Ap app;

	public SavedPhrasesListLayout(Context context) {
		super(context);
	}

	public void bind(SubItem subtitle, TranslationUnit translationUnit) {
		long startTime = subtitle.getStartDate().getTime();
		String originalText = subtitle.getText().substring(translationUnit.getStartIndex(), translationUnit.getEndIndex());
		String translatedText = "<b>" + translationUnit.getTranslation() + "</b>";
		String context = subtitle.getText().substring(0, translationUnit.getStartIndex()) + "<u>" + originalText + "</u>" + subtitle.getText().substring(translationUnit.getEndIndex());

		this.startTime.setText(Util.stringForTime((int) startTime));
		this.originalText.setText(originalText);
		this.context.setText(Html.fromHtml(context));
		this.translatedText.setText(Html.fromHtml(translatedText));
	}

}
