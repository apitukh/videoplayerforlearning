/*****************************************************************************
 * MediaDatabase.java
 *****************************************************************************
 * Copyright © 2011-2012 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package com.brothers.pitukh.videoviewdemo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

public class MediaDatabase {
	Context context;

	public final static String TAG = "VLC/MediaDatabase";

	private static MediaDatabase instance;

	private SQLiteDatabase mDb;
	private final String DB_NAME = "medias";
	private final int DB_VERSION = 1;
	private final int CHUNK_SIZE = 50;

	private final String MEDIA_TABLE_NAME = "media_table";
	private final String MEDIA_LOCATION = "location";
	private final String MEDIA_TIME = "time";
	private final String MEDIA_LENGTH = "length";
	private final String MEDIA_TYPE = "type";
	private final String MEDIA_PICTURE = "picture";
	private final String MEDIA_TITLE = "title";
	private final String MEDIA_WIDTH = "width";
	private final String MEDIA_HEIGHT = "height";

	public enum mediaColumn {
		MEDIA_TABLE_NAME, MEDIA_PATH, MEDIA_TIME, MEDIA_LENGTH, MEDIA_TYPE, MEDIA_PICTURE, MEDIA_TITLE, MEDIA_WIDTH, MEDIA_HEIGHT
	}

	private MediaDatabase(Context context) {
		this.context = context;
		// create or open database
		DatabaseHelper helper = new DatabaseHelper(context);
		this.mDb = helper.getWritableDatabase();
	}

	public synchronized static MediaDatabase getInstance(Context context) {
		if (instance == null) {
			instance = new MediaDatabase(context.getApplicationContext());
		}
		return instance;
	}

	private class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {
			super(context, DB_NAME, null, DB_VERSION);
		}

		public void dropMediaTableQuery(SQLiteDatabase db) {
			String query = "DROP TABLE " + MEDIA_TABLE_NAME + ";";
			db.execSQL(query);
		}

		public void createMediaTableQuery(SQLiteDatabase db) {
			String query = "CREATE TABLE IF NOT EXISTS " + MEDIA_TABLE_NAME + " (" + MEDIA_LOCATION + " TEXT PRIMARY KEY NOT NULL, "
					+ MEDIA_TIME + " INTEGER, " + MEDIA_LENGTH + " INTEGER, " + MEDIA_TYPE + " INTEGER, " + MEDIA_PICTURE + " BLOB, "
					+ MEDIA_TITLE + " VARCHAR(200), " + MEDIA_WIDTH + " INTEGER, " + MEDIA_HEIGHT + " INTEGER" + ");";
			db.execSQL(query);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			createMediaTableQuery(db);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if (oldVersion < DB_VERSION && newVersion == DB_VERSION) {
				dropMediaTableQuery(db);
				createMediaTableQuery(db);
			}
		}
	}

	/**
	 * Add a new media to the database. The picture can only added by update.
	 * 
	 * @param media
	 *            which you like to add to the database
	 */
	public synchronized void addMedia(Media media) {

		ContentValues values = new ContentValues();

		values.put(MEDIA_LOCATION, media.getPath());
		values.put(MEDIA_TIME, media.getTime());
		values.put(MEDIA_LENGTH, media.getDuration());
		values.put(MEDIA_TYPE, media.getType());
		values.put(MEDIA_TITLE, media.getTitle());
		values.put(MEDIA_WIDTH, media.getWidth());
		values.put(MEDIA_HEIGHT, media.getHeight());

		mDb.replace(MEDIA_TABLE_NAME, "NULL", values);

	}

	/**
	 * Check if the item is already in the database
	 * 
	 * @param location
	 *            of the item (primary key)
	 * @return True if the item exists, false if it does not
	 */
	public synchronized boolean mediaItemExists(String location) {
		try {
			Cursor cursor = mDb.query(MEDIA_TABLE_NAME, new String[] { MEDIA_LOCATION }, MEDIA_LOCATION + "=?", new String[] { location },
					null, null, null);
			boolean exists = cursor.moveToFirst();
			cursor.close();
			return exists;
		} catch (Exception e) {
			Log.e(TAG, "Query failed");
			return false;
		}
	}

	/**
	 * Get all paths from the items in the database
	 * 
	 * @return list of File
	 */
	@SuppressWarnings("unused")
	private synchronized HashSet<File> getMediaFiles() {

		HashSet<File> files = new HashSet<File>();
		Cursor cursor;

		cursor = mDb.query(MEDIA_TABLE_NAME, new String[] { MEDIA_LOCATION }, null, null, null, null, null);
		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			do {
				File file = new File(cursor.getString(0));
				files.add(file);
			} while (cursor.moveToNext());
		}
		cursor.close();

		return files;
	}

	public synchronized HashMap<String, Media> getMedias() {

		DatabaseHelper helper = new DatabaseHelper(context);
		this.mDb = helper.getWritableDatabase();

		Cursor cursor;
		HashMap<String, Media> medias = new HashMap<String, Media>();
		int chunk_count = 0;
		int count = 0;

		do {
			count = 0;
			cursor = mDb.rawQuery(String.format(Locale.US, "SELECT %s,%s,%s,%s,%s,%s,%s FROM %s LIMIT %d OFFSET %d", MEDIA_TIME, // 0 long
					MEDIA_LENGTH, // 1 long
					MEDIA_TYPE, // 2 int
					MEDIA_TITLE, // 3 string
					MEDIA_WIDTH, // 4 int
					MEDIA_HEIGHT, // 5 int
					MEDIA_LOCATION, // 6 string
					MEDIA_TABLE_NAME, CHUNK_SIZE, chunk_count * CHUNK_SIZE), null);

			if (cursor.moveToFirst()) {
				do {
					String location = cursor.getString(6);
					Media media = new Media(location, cursor.getLong(0), // MEDIA_TIME
							cursor.getLong(1), // MEDIA_LENGTH
							cursor.getInt(2), // MEDIA_TYPE
							null, // MEDIA_PICTURE
							cursor.getString(3), // MEDIA_TITLE
							cursor.getInt(4), // MEDIA_WIDTH
							cursor.getInt(5)); // MEDIA_HEIGHT
					medias.put(media.getPath(), media);

					count++;
				} while (cursor.moveToNext());
			}

			cursor.close();
			chunk_count++;
		} while (count == CHUNK_SIZE);

		return medias;
	}

	public synchronized HashMap<String, Long> getVideoTimes() {

		Cursor cursor;
		HashMap<String, Long> times = new HashMap<String, Long>();
		int chunk_count = 0;
		int count = 0;

		do {
			count = 0;
			cursor = mDb.rawQuery(String.format(Locale.US, "SELECT %s,%s FROM %s WHERE %s=%d LIMIT %d OFFSET %d", MEDIA_LOCATION, // 0
																																	// string
					MEDIA_TIME, // 1 long
					MEDIA_TABLE_NAME, MEDIA_TYPE,
					// Media.TYPE_VIDEO,
					CHUNK_SIZE, chunk_count * CHUNK_SIZE), null);

			if (cursor.moveToFirst()) {
				do {
					String location = cursor.getString(0);
					long time = cursor.getLong(1);
					times.put(location, time);
					count++;
				} while (cursor.moveToNext());
			}

			cursor.close();
			chunk_count++;
		} while (count == CHUNK_SIZE);

		return times;
	}

	public synchronized Media getMedia(String location) {

		Cursor cursor;
		Media media = null;

		cursor = mDb.query(MEDIA_TABLE_NAME, new String[] { MEDIA_TIME, // 0 long
				MEDIA_LENGTH, // 1 long
				MEDIA_TYPE, // 2 int
				MEDIA_TITLE, // 3 string
				MEDIA_WIDTH, // 7 int
				MEDIA_HEIGHT // 8 int
				}, MEDIA_LOCATION + "=?", new String[] { location }, null, null, null);
		if (cursor.moveToFirst()) {
			media = new Media(location, cursor.getLong(0), // MEDIA_TIME
					cursor.getLong(1), // MEDIA_LENGTH
					cursor.getInt(2), // MEDIA_TYPE
					null, // MEDIA_PICTURE
					cursor.getString(3), // MEDIA_TITLE
					cursor.getInt(7), // MEDIA_WIDTH
					cursor.getInt(8)); // MEDIA_HEIGHT
		}
		cursor.close();
		return media;
	}

	public synchronized Bitmap getPicture(String location) {
		/* Used for the lazy loading */
		Cursor cursor;
		Bitmap picture = null;
		byte[] blob;

		cursor = mDb.query(MEDIA_TABLE_NAME, new String[] { MEDIA_PICTURE }, MEDIA_LOCATION + "=?", new String[] { location }, null, null,
				null);
		if (cursor.moveToFirst()) {
			blob = cursor.getBlob(0);
			if (blob != null && blob.length > 1 && blob.length < 500000) {
				picture = BitmapFactory.decodeByteArray(blob, 0, blob.length);
				blob = null;
			}
		}
		cursor.close();
		return picture;
	}

	public synchronized void removeMedia(String location) {
		mDb.delete(MEDIA_TABLE_NAME, MEDIA_LOCATION + "=?", new String[] { location });
	}

	public void removeMedias(Set<String> locations) {
		mDb.beginTransaction();
		try {
			for (String location : locations)
				mDb.delete(MEDIA_TABLE_NAME, MEDIA_LOCATION + "=?", new String[] { location });
			mDb.setTransactionSuccessful();
		} finally {
			mDb.endTransaction();
		}
	}

	public synchronized void updateMedia(String location, mediaColumn col, Object object) {

		if (location == null)
			return;

		ContentValues values = new ContentValues();
		switch (col) {
		case MEDIA_PICTURE:
			if (object != null) {
				Bitmap picture = (Bitmap) object;
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				picture.compress(Bitmap.CompressFormat.JPEG, 90, out);
				values.put(MEDIA_PICTURE, out.toByteArray());
			} else {
				values.put(MEDIA_PICTURE, new byte[1]);
			}
			break;
		case MEDIA_TIME:
			if (object != null)
				values.put(MEDIA_TIME, (Long) object);
			break;
		case MEDIA_LENGTH:
			if (object != null)
				values.put(MEDIA_LENGTH, (Long) object);
			break;
		default:
			return;
		}
		mDb.update(MEDIA_TABLE_NAME, values, MEDIA_LOCATION + "=?", new String[] { location });
	}

	/**
	 * Empty the database for debugging purposes
	 */
	public synchronized void emptyDatabase() {
		mDb.delete(MEDIA_TABLE_NAME, null, null);
	}
}
