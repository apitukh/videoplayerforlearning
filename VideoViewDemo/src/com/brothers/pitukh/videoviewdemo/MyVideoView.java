package com.brothers.pitukh.videoviewdemo;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.VideoView;

import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EView;

@EView
public class MyVideoView extends VideoView {

	VideoPlayerActivity_ activity;

	public MyVideoView(Context context) {
		super(context);
		activity = (VideoPlayerActivity_) getContext();
	}

	public MyVideoView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		activity = (VideoPlayerActivity_) getContext();
	}

	public MyVideoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		activity = (VideoPlayerActivity_) getContext();
	}

	@Override
	public void start() {
		super.start();
		int mustGoTo = getCurrentPosition() >= 0 ? getCurrentPosition() : 0;
		if (activity.subtitleManager != null) {
			activity.subtitleManager.start(mustGoTo);
		}
		if (afterSeek) {
			isWrongCurrentPositionAfter(mustGoTo);
		}
		activity.startUpdatingTimeBar();
		activity.updatePlayPause();
	}

	@Background
	void isWrongCurrentPositionAfter(int sleepFrom) {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int diff = Math.abs(sleepFrom - getCurrentPosition());
		//Log.v(Ap.TAG, "" + diff);
		if (diff > 110) {
			Log.v(Ap.TAG, "Violation!");
			if (activity.subtitleManager != null) {
				activity.subtitleManager.start(getCurrentPosition());
			}
			activity.updateTimeBar();
			activity.startUpdatingTimeBar();
			Log.v(Ap.TAG, "Violation end");
		}
		afterSeek = false;
	}

	private boolean afterSeek = false;

	@Override
	public void seekTo(int msec) {
		super.seekTo(msec);
		if (activity.subtitleManager != null) {
			if (isPlaying()) {
				activity.subtitleManager.stop();
				activity.subtitleManager.start(msec);
			} else {
				activity.subtitleManager.start(msec);
				activity.subtitleManager.stop();
			}
		}
		activity.updatePlayPause();
		if (!activity.isDragged) {
			activity.updateTimeBar();
		}
		if (isPlaying()) {
			isWrongCurrentPositionAfter(msec);
		} else {
			afterSeek = true;
		}
	}

	@Override
	public void pause() {
		super.pause();
		if (activity.subtitleManager != null) {
			activity.subtitleManager.stop();
		}
		activity.updatePlayPause();
	}

}
