package com.brothers.pitukh.videoviewdemo;

import java.io.IOException;
import java.util.List;

import org.holoeverywhere.widget.EditText;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.brothers.pitukh.videoviewdemo.model.TranslationUnit;
import com.brothers.pitukh.videoviewdemo.subtitles.Subtitles;
import com.brothers.pitukh.videoviewdemo.subtitles.Subtitles.SubItem;

public class SavedPhrasesAdapter extends BaseAdapter {

	private final Context context;
	private List<TranslationUnit> translations;
	private String subtitleFile;
	private Subtitles subs;

	Ap app;

	public SavedPhrasesAdapter(Context context, String subtitleFile) throws IOException {
		super();
		this.context = context;
		this.subtitleFile = subtitleFile;
		app = (Ap) ((Activity) context).getApplication();
		this.translations = app.dataSource.getAllTranslations(subtitleFile);
		this.subs = Subtitles.from(subtitleFile);
	}

	Subtitles getSubtitles() {
		return this.subs;
	}

	public void add(TranslationUnit newUnit) {
		Util.checkOnMainThread();
		if (!translations.contains(newUnit)) {
			if (app.dataSource.addTranslation(subtitleFile, newUnit)) {
				translations.add(newUnit);
			}
		}
	}

	public void remove(int position) {
		Util.checkOnMainThread();
		if (app.dataSource.remove(subtitleFile, getItem(position))) {
			this.translations.remove(position);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SavedPhrasesListLayout resultView;
		if (convertView == null) {
			resultView = SavedPhrasesListLayout_.build(context);
		} else {
			resultView = (SavedPhrasesListLayout) convertView;
		}
		TranslationUnit translationUnit = getItem(position);
		SubItem subtitle = subs.getItemAt(translationUnit.getSubtitleNumber());
		resultView.bind(subtitle, translationUnit);
		return resultView;
	}

	@Override
	public int getCount() {
		return translations.size();
	}

	@Override
	public TranslationUnit getItem(int position) {
		return translations.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

}
