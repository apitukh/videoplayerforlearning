package com.brothers.pitukh.videoviewdemo;

import group.pals.android.lib.ui.filechooser.FileChooserActivity;
import group.pals.android.lib.ui.filechooser.io.localfile.LocalFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.widget.Button;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.LinearLayout;
import org.holoeverywhere.widget.SeekBar;
import org.holoeverywhere.widget.SeekBar.OnSeekBarChangeListener;
import org.holoeverywhere.widget.TextView;
import org.holoeverywhere.widget.Toast;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.text.ClipboardManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.brothers.pitukh.videoviewdemo.Util.WordBounds;
import com.brothers.pitukh.videoviewdemo.model.TranslationUnit;
import com.brothers.pitukh.videoviewdemo.subtitles.SubtitleManager;
import com.brothers.pitukh.videoviewdemo.subtitles.SubtitleManager.ISubtitleListener;
import com.brothers.pitukh.videoviewdemo.subtitles.Subtitles.SubItem;
import com.google.common.collect.Maps;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.RangeSet;
import com.google.common.collect.TreeRangeMap;
import com.google.common.collect.TreeRangeSet;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.App;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.LongClick;
import com.googlecode.androidannotations.annotations.Touch;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity(R.layout.videoplayer)
public class VideoPlayerActivity extends org.holoeverywhere.app.Activity implements ISubtitleListener {

	@ViewById(R.id.subtitlesButton)
	protected ImageButton subtitlesButton;
	@ViewById(R.id.imageView1)
	protected ImageView bookButton;
	@ViewById(R.id.buttonPlayPause)
	protected ImageButton buttonPlayPause;
	@ViewById(R.id.startTime)
	protected TextView startTime;
	@ViewById(R.id.endTime)
	protected TextView endTime;
	@ViewById(R.id.videoview1)
	protected MyVideoView mVideoView;
	@ViewById(R.id.textView1)
	protected TextView mTextView;
	@ViewById(R.id.timeBar)
	protected SeekBar timeBar;
	@ViewById(R.id.buttonPrevSub)
	protected ImageButton buttonPrevSub;
	@ViewById(R.id.buttonNextSub)
	protected ImageButton buttonNextSub;
	@ViewById(R.id.buttonReplaySub)
	protected ImageButton buttonReplaySub;

	@ViewById(R.id.blockPlace)
	protected LinearLayout blockPlace;
	@ViewById(R.id.buttonsBlock)
	protected LinearLayout buttonsBlock;
	@ViewById(R.id.timeBarBlock)
	protected LinearLayout timeBarBlock;
	@ViewById(R.id.editTranslationBlock)
	protected LinearLayout editTranslationBlock;

	@ViewById(R.id.contextualBlock)
	LinearLayout contextualBlock;
	@ViewById(R.id.addButton)
	Button addButton;
	@ViewById(R.id.clearButton)
	Button clearButton;
	@ViewById(R.id.translateButton)
	Button translateButton;
	@ViewById(R.id.editTranslationButton)
	Button editTranslationButton;
	@ViewById(R.id.removeTranslationButton)
	Button removeTranslationButton;

	@App
	protected Ap app;

	@Extra(EXTRA_VIDEOPLAYER_FILE_PATH)
	protected String videoFilePath;
	@Extra(EXTRA_VIDEOPLAYER_START)
	protected boolean startVideo;
	@Extra(EXTRA_VIDEOPLAYER_START_FROM)
	protected long playFrom = 0;

	private long videoDuration;

	public static final String EXTRA_VIDEOPLAYER_FILE_PATH = "Videoplayer_FilePath";
	public static final String EXTRA_VIDEOPLAYER_START = "Videoplayer_Start";
	public static final String EXTRA_VIDEOPLAYER_START_FROM = "Videoplayer_StartFrom";

	private static final int _ReqChooseSubs = 1;
	// private int lastVideoPosition = -1;

	SubtitleManager subtitleManager;

	boolean isDragged = false;

	private static final String SAVED_STATE_PLAY_FROM = "SavedStatePlayFrom";
	private static final String SAVED_STATE_START = "SavedStateStart";

	private Map<Integer, RangeMap<Integer, TranslationUnit>> translations = Maps.newHashMap();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey(SAVED_STATE_PLAY_FROM)) {
				playFrom = savedInstanceState.getLong(SAVED_STATE_PLAY_FROM);
			}
			if (savedInstanceState.containsKey(SAVED_STATE_START)) {
				startVideo = savedInstanceState.getBoolean(SAVED_STATE_START);
			}
		}
	}

	@AfterViews
	public void after() {
		// TODO: do I have to set it also in other activities?
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		Util.setAlpha(subtitlesButton, 0.9f);

		// Time bar configuration
		final SeekBar view = ((SeekBar) findViewById(R.id.timeBar));
		if (view != null) {
			view.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {
					VideoPlayerActivity.this.timeBarTouchStop();
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {
					VideoPlayerActivity.this.timeBarTouchStart();
				}

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					VideoPlayerActivity.this.timeBarProgressChange(seekBar, progress);
				}
			});
		}

		File videoFile = new File(videoFilePath);
		if (!videoFile.isFile()) {
			// TODO: do something with this
			Toast.makeText(this, "This is not a file", Toast.LENGTH_LONG).show();
		}

		// Tuning subtitles
		File subsPath = null;
		SharedPreferences settings = getSharedPreferences(ConstantsShared.SHARED_VIDEO_TO_SUBS, MODE_PRIVATE);
		if (settings.contains(videoFilePath)) {
			String subsPathString = settings.getString(videoFilePath, null);
			if (subsPathString != null) {
				subsPath = new File(subsPathString);
			}
		} else {
			String file = videoFile.getName();
			String newFile = file.substring(0, file.length() - 4) + ".srt";
			File similarSubFile = new File(videoFile.getParentFile(), newFile);
			if (similarSubFile.exists()) {
				settings.edit().putString(videoFilePath, similarSubFile.getAbsolutePath()).commit();
				subsPath = similarSubFile;
			}
		}
		try {
			if (subsPath != null) {
				subtitleManager = new SubtitleManager(subsPath, this, mVideoView);
			}
		} catch (IOException e) {
			Log.e(Ap.TAG, "Unable to create subtitleManager", e);
		}
		configureSubtitlesLook();
		textView2.setVisibility(View.INVISIBLE);
		if (isPortrait()) {
			showTimeBarBlockOnly();
		} else {
			if (startVideo) {
				hideBlocks();
			} else {
				showTimeBarBlockOnly();
			}
		}

		mVideoView.setMediaController(null);

		// Get translations
		if (subsPath != null) {
			for (TranslationUnit unit : app.dataSource.getAllTranslations(subsPath)) {
				int subnumber = unit.getSubtitleNumber();
				RangeMap<Integer, TranslationUnit> map;
				if (translations.containsKey(subnumber)) {
					map = translations.get(subnumber);
				} else {
					map = TreeRangeMap.create();
				}
				map.put(Range.closed(unit.getStartIndex(), unit.getEndIndex()), unit);
				translations.put(subnumber, map);
			}
		}

		videoDuration = Util.getVideoDuration(this, videoFilePath);
		changeTextView(endTime, Util.stringForTime((int) videoDuration));
		timeBar.setMax(((int) videoDuration) / 1000);
		mVideoView.setOnCompletionListener(new OnCompletionListener() {
			@Override
			public void onCompletion(MediaPlayer mp) {
				updateTimeBar();
			}
		});
		mVideoView.setOnPreparedListener(new OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer arg0) {
				subtitlesButton.setVisibility(View.VISIBLE);
				bookButton.setVisibility(View.VISIBLE);
			}
		});
		if (isPortrait()) {
			subtitlesButton.setVisibility(View.GONE);
			bookButton.setVisibility(View.GONE);
		}
	}

	@Override
	protected void onPause() {
		Log.v(Ap.TAG, "onPause start");
		super.onPause();
		mVideoView.pause();
		playFrom = mVideoView.getCurrentPosition();
		Log.v(Ap.TAG, "playFrom: " + playFrom);
		Log.v(Ap.TAG, "onPause end");
	}

	@Override
	protected void onResume() {
		Log.v(Ap.TAG, "onResume start");
		Log.v(Ap.TAG, "startVideo: " + startVideo);
		Log.v(Ap.TAG, "lastVideoPosition: " + playFrom);
		super.onResume();
		if (videoFilePath != null) {
			if (playFrom == -1) {
				prepareVideo(videoFilePath, 0);
			} else {
				prepareVideo(videoFilePath, (int) playFrom);
			}
		}
		if (startVideo) {
			mVideoView.start();
			startVideo = false;
		}
		Log.v(Ap.TAG, "onResume end");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mVideoView.stopPlayback();
	}

	@UiThread
	void changeTextView(TextView tv, String newValue) {
		tv.setText(newValue);
	}

	@UiThread
	void changeTextView(TextView tv, Spannable newValue) {
		tv.setText(newValue);
	}

	@LongClick(R.id.subtitlesButton)
	void openSubsClicked() {
		mVideoView.pause();
		Intent intent = new Intent(this, FileChooserActivity.class);
		intent.putExtra(FileChooserActivity._Rootpath, (Parcelable) new LocalFile("/your/path"));
		intent.putExtra(FileChooserActivity._RegexFilenameFilter, "(?si).*\\.srt$");
		intent.putExtra(FileChooserActivity._Theme, android.R.style.Theme_Dialog);
		intent.putExtra(FileChooserActivity._MultiSelection, false);
		startActivityForResult(intent, _ReqChooseSubs);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case _ReqChooseSubs:
			if (resultCode == RESULT_OK) {
				List<LocalFile> files = (List<LocalFile>) data.getSerializableExtra(FileChooserActivity._Results);
				if (files.size() == 1) {
					SharedPreferences settings = getSharedPreferences(ConstantsShared.SHARED_VIDEO_TO_SUBS, MODE_WORLD_WRITEABLE);
					settings.edit().putString(videoFilePath, files.get(0).getAbsolutePath()).commit();
					if (subtitleManager != null) {
						subtitleManager.setSubsFile(files.get(0));
					} else {
						try {
							subtitleManager = new SubtitleManager(files.get(0), this, mVideoView);
						} catch (IOException e) {
							Log.e(Ap.TAG, "SubtitleManager", e);
						}
					}
				}
				configureSubtitlesLook();
				textView2.setVisibility(View.INVISIBLE);
				buttonsBlock.setVisibility(View.VISIBLE);
			}
			break;
		}
	}

	@Click(R.id.buttonPlayPause)
	void playPauseClicked() {
		playPauseClicked(true);
	}

	void playPauseClicked(boolean showTimeBar) {
		if (mVideoView.isPlaying()) {
			mVideoView.pause();
			buttonsBlock.setVisibility(View.VISIBLE);
		} else {
			mVideoView.start();
			buttonsBlock.setVisibility(View.INVISIBLE);
		}
		textView2.setVisibility(View.INVISIBLE);
		if (showTimeBar) {
			showTimeBarBlockOnly();
		}
		updatePlayPause();
	}

	@UiThread
	void updatePlayPause() {
		textView2.setVisibility(View.INVISIBLE);
		if (mVideoView.isPlaying()) {
			if (isPortrait()) {
				showTimeBarBlockOnly();
			} else {
				hideBlocks();
			}
			buttonPlayPause.setImageResource(R.drawable.av_pause);
		} else {
			// !!! DO NOT UNCOMMENT THESE LINES !!! (error occurs)
			// timeBarBlock.setVisibility(View.VISIBLE);
			// buttonsBlock.setVisibility(View.VISIBLE);
			buttonPlayPause.setImageResource(R.drawable.av_play);
		}
	}

	void updateTimeBar() {
		int currentPos = mVideoView.getCurrentPosition();
		timeBar.setProgress(currentPos / 1000);
		changeTextView(startTime, Util.stringForTime(currentPos));
	}

	private boolean isVisible = true;

	@Click(R.id.subtitlesButton)
	void toggleSubClicked() {
		if (isVisible) {
			changeTextView(mTextView, (String) null);
			Util.setAlpha(subtitlesButton, 0.3f);
			isVisible = false;
		} else {
			if (subtitleIsPlaying && subtitleManager != null) {
				changeTextView(mTextView, subtitleManager.getCurrentSubtitle().getText());
			}
			Util.setAlpha(subtitlesButton, 0.9f);
			isVisible = true;
		}
	}

	// TODO: currently, AA cannot work with HoloEverywhere's SeekBar.
	// In new release (AA 3.0) it can be done.
	// @SeekBarProgressChange(R.id.timeBar)
	void timeBarProgressChange(SeekBar seekBar, int progress) {
		if (isDragged) {
			mVideoView.seekTo(progress * 1000);
			changeTextView(startTime, Util.stringForTime(progress * 1000));
		}
	}

	// @SeekBarTouchStart(R.id.timeBar)
	void timeBarTouchStart() {
		isDragged = true;
	}

	// @SeekBarTouchStop(R.id.timeBar)
	void timeBarTouchStop() {
		isDragged = false;
	}

	@Touch(R.id.videoview1)
	void videoTouched(MotionEvent motionEvent) {
		if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
			playPauseClicked(true);
		}
	}

	private void prepareVideo(String path, int msec) {
		mVideoView.setVideoPath(path);
		mVideoView.requestFocus();
		mVideoView.seekTo(msec);
		timeBar.setProgress(msec / 1000);
		updateTimeBar();
	}

	void startUpdatingTimeBar() {
		mHandler.removeCallbacksAndMessages(null);
		mHandler.sendEmptyMessageDelayed(12345, 1000);
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (mVideoView.isPlaying()) {
				timeBar.setProgress(timeBar.getProgress() + 1);
				changeTextView(startTime, Util.stringForTime(timeBar.getProgress() * 1000));
				sendMessageDelayed(obtainMessage(), 1000);
			}
		}
	};

	@Override
	public void subtitleStarted(SubItem newItem) {
		if (isVisible) {
			int subnumber = subtitleManager.getCurrentSubtitleNumber();
			Spannable spantext = new SpannableStringBuilder(newItem.getText());
			if (translations.containsKey(subnumber)) {
				for (TranslationUnit unit : translations.get(subnumber).asMapOfRanges().values()) {
					spantext.setSpan(new ForegroundColorSpan(Color.WHITE), unit.getStartIndex(), unit.getEndIndex(),
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					spantext.setSpan(new BackgroundColorSpan(Color.BLUE), unit.getStartIndex(), unit.getEndIndex(),
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
			}
			changeTextView(mTextView, spantext);
			this.subtitleIsPlaying = true;
			selectionStart = -1;
			selectionEnd = -1;
		}
	}

	@Override
	public void subtitleFinished() {
		changeTextView(mTextView, (String) null);
		this.subtitleIsPlaying = false;
		selectionStart = -1;
		selectionEnd = -1;
	}

	// TODO: refactor!
	private boolean isReplayed = false;
	private boolean subtitleIsPlaying = false;

	@Click(R.id.buttonPrevSub)
	void prevSubClicked() {
		// TODO: make button inactive, when there is no subtitle yet
		if (subtitleManager != null) {
			int currentSubtitleNumber = subtitleManager.getCurrentSubtitleNumber();
			if (currentSubtitleNumber - 1 >= 0) {
				mVideoView.seekTo((int) subtitleManager.getSubtitles().getItemAt(currentSubtitleNumber - 1).getStartDate().getTime());
			} else {
				mVideoView.seekTo((int) subtitleManager.getSubtitles().getItemAt(0).getStartDate().getTime());
			}
		} else {
			int seekTime = mVideoView.getCurrentPosition() - 5000;
			if (seekTime < 0) {
				mVideoView.seekTo(0);
			} else {
				mVideoView.seekTo(seekTime);
			}
		}
		textView2.setVisibility(View.INVISIBLE);
		showTimeBarBlockOnly();
		isReplayed = false;
	}

	@ViewById(R.id.imageRefreshed)
	protected ImageView imageRefreshed;

	@Click(R.id.buttonNextSub)
	void nextSubClicked() {
		if (subtitleManager != null) {
			int currentSubtitleNumber = subtitleManager.getCurrentSubtitleNumber();
			int seekTime = -1;
			if (subtitleIsPlaying && currentSubtitleNumber + 1 <= subtitleManager.getSubtitles().getSize()) {
				seekTime = (int) subtitleManager.getSubtitles().getItemAt(currentSubtitleNumber + 1).getStartDate().getTime();
			} else {
				seekTime = (int) subtitleManager.getSubtitles().getItemAt(currentSubtitleNumber).getStartDate().getTime();
			}
			mVideoView.seekTo(seekTime);
		} else {
			int seekTime = mVideoView.getCurrentPosition() + 5000;
			if (seekTime >= videoDuration) {
				mVideoView.seekTo((int) videoDuration - 1);
			} else {
				mVideoView.seekTo(seekTime);
			}
		}
		textView2.setVisibility(View.INVISIBLE);
		showTimeBarBlockOnly();
		isReplayed = false;
	}

	@Click(R.id.buttonReplaySub)
	void replaySubClicked() {
		if (subtitleManager != null) {
			int currentSubtitleNumber = subtitleManager.getCurrentSubtitleNumber();
			int subNumberToSeek = 0;
			if (subtitleIsPlaying) {
				subNumberToSeek = currentSubtitleNumber;
			} else {
				subNumberToSeek = currentSubtitleNumber - 1;
			}
			if (subNumberToSeek < 0) {
				subNumberToSeek = 0;
			}
			isReplayed = true;
			mVideoView.seekTo((int) subtitleManager.getSubtitles().getItemAt(subNumberToSeek).getStartDate().getTime());
			textView2.setVisibility(View.INVISIBLE);
			buttonsBlock.setVisibility(View.INVISIBLE);
			if (isPortrait()) {
				showTimeBarBlockOnly();
			} else {
				hideBlocks();
			}
			mVideoView.start();
		}
	}

	// TODO: make 2 private fields instead of this method
	private boolean isPortrait() {
		return getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
	}

	private void configureSubtitlesLook() {
		if (subtitleManager == null) {
			bookButton.setEnabled(false);
			buttonReplaySub.setEnabled(false);
			String subtitlesTurnedOff = getString(R.string.player_subtitles_off);
			Toast.makeText(this, subtitlesTurnedOff, Toast.LENGTH_LONG).show();
		} else {
			bookButton.setEnabled(true);
			buttonReplaySub.setEnabled(true);
			String subtitlesTurnedOn = getString(R.string.player_subtitles_on);
			Toast.makeText(this, subtitlesTurnedOn + ": " + subtitleManager.getSubtitleFile().getName(), Toast.LENGTH_LONG).show();
		}
	}

	private int subtitleClickedPositionDown = -1;
	private int subtitleClickedPositionUp = -1;
	private int selectionStart = -1; // Inclusive
	private int selectionEnd = -1; // Exclusive
	private Range<Integer> allowedRange;

	private void beforeTouchFired(MotionEvent event) {
		if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
			subtitleClickedPositionDown = -1;
			subtitleClickedPositionUp = -1;
			// TODO: this must be called only once per subtitle item (when subtitle is changing)
			if (!translations.containsKey(subtitleManager.getCurrentSubtitleNumber())) {
				allowedRange = null;
			}
			if (selectionStart == -1 && selectionEnd == -1) {
				allowedRange = null;
			}
		}
	}

	@Touch(R.id.textView1)
	public boolean touchSubText(MotionEvent event) {
		beforeTouchFired(event);

		if (mVideoView.isPlaying()) {
			playPauseClicked(isPortrait());
		}

		boolean noSelection = ((selectionStart == -1) && (selectionEnd == -1));
		boolean isDown = event.getActionMasked() == MotionEvent.ACTION_DOWN;
		boolean isUp = event.getActionMasked() == MotionEvent.ACTION_UP;

		CharSequence formattedText = mTextView.getText();
		String unformattedText = formattedText.toString();

		int position = Util.getOffsetForPosition(mTextView, event.getX(), event.getY());
		if (position < 0) {
			int possiblePosition = Util.getOffsetAtCoordinate(mTextView, 0, event.getX());
			if (possiblePosition > 0) {
				position = possiblePosition;
			}
		}
		if (position > unformattedText.length()) {
			int possiblePosition = Util.getOffsetAtCoordinate(mTextView, mTextView.getLineCount() - 1, event.getX());
			if (possiblePosition < unformattedText.length()) {
				position = possiblePosition;
			}
		}

		if (isDown) {
			subtitleClickedPositionDown = position;
		}
		if (isUp) {
			subtitleClickedPositionUp = position;
		}

		if (allowedRange != null && !allowedRange.contains(position)) {
			if (isUp) {
				savedPhraseClicked();
			}
			return true;
		}

		if (noSelection) {
			roundBounds(unformattedText, position);
		} else if (isUp) {
			roundBounds(unformattedText, position);
			roundBounds(unformattedText, selectionStart);
			roundBounds(unformattedText, selectionEnd - 1);
		} else {
			boolean goodPosition = (position >= 0) && (position < mTextView.getText().length());
			if (position < selectionStart && goodPosition) {
				selectionStart = position;
			}
			if (selectionEnd < position && goodPosition) {
				selectionEnd = position;
			}
		}

		if (noSelection && translations.get(subtitleManager.getCurrentSubtitleNumber()) != null) {
			allowedRange = null;
			if (selectionStart != -1 || selectionEnd != -1) {
				if (translations.get(subtitleManager.getCurrentSubtitleNumber()).get(position) == null) {
					// Get range that contains current position (selectionStart)
					RangeSet<Integer> tempRangeSet = TreeRangeSet.create();
					for (Range<Integer> range : translations.get(subtitleManager.getCurrentSubtitleNumber()).asMapOfRanges().keySet()) {
						tempRangeSet.add(range);
					}
					tempRangeSet = tempRangeSet.complement();

					allowedRange = tempRangeSet.rangeContaining(position);
				} else {
					// User touched position that is already taken by another phrase
					selectionStart = -1;
					selectionEnd = -1;
					if (isUp) {
						savedPhraseClicked();
					}
					return true;
				}
			}
		}

		Spannable spantext = new SpannableStringBuilder(formattedText);
		spantext.setSpan(new ForegroundColorSpan(Color.BLACK), selectionStart, selectionEnd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
		spantext.setSpan(new BackgroundColorSpan(Color.WHITE), selectionStart, selectionEnd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

		Log.v(Ap.TAG, spantext.toString());
		mTextView.setText(spantext);

		textView2.setVisibility(View.INVISIBLE);
		if (isPortrait() || (!isPortrait() && isUp)) {
			showContextualBlockOnly();
		}
		return true;
	}

	private void roundBounds(String unformattedText, int position) {
		if (position < 0) {
			position = 0;
		}
		WordBounds wordBounds = Util.getWordBoundsFromText(unformattedText, position);
		if (selectionStart == -1 || wordBounds.startIndex < selectionStart) {
			selectionStart = wordBounds.startIndex;
		}
		if (selectionEnd == -1 || wordBounds.endIndex + 1 > selectionEnd) {
			selectionEnd = wordBounds.endIndex + 1;
		}
	}

	@ViewById(R.id.textView2)
	TextView textView2;

	int savedPhraseClicked;

	protected void savedPhraseClicked() {
		int subnumber = subtitleManager.getCurrentSubtitleNumber();
		if (translations.containsKey(subnumber)) {
			TranslationUnit tuDown = translations.get(subnumber).get(subtitleClickedPositionDown);
			TranslationUnit tuUp = translations.get(subnumber).get(subtitleClickedPositionUp);
			if (tuDown != null && tuUp != null && tuDown == tuUp) {
				if (textView2.getVisibility() == View.VISIBLE) {
					textView2.setVisibility(View.INVISIBLE);
					showTimeBarBlockOnly();
				} else {
					textView2.setVisibility(View.VISIBLE);
					showEditBlockOnly();
					savedPhraseClicked = subtitleClickedPositionDown;
				}
				textView2.setText(tuUp.getTranslation());
			}
		}
		subtitleClickedPositionDown = -1;
		subtitleClickedPositionUp = -1;
	}

	@Click(R.id.clearButton)
	protected void clearClicked() {
		int subnumber = subtitleManager.getCurrentSubtitleNumber();
		Spannable spantext = new SpannableStringBuilder(mTextView.getText().toString());
		if (translations.containsKey(subnumber)) {
			for (TranslationUnit unit : translations.get(subnumber).asMapOfRanges().values()) {
				spantext.setSpan(new ForegroundColorSpan(Color.WHITE), unit.getStartIndex(), unit.getEndIndex(),
						Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				spantext.setSpan(new BackgroundColorSpan(Color.BLUE), unit.getStartIndex(), unit.getEndIndex(),
						Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
		}
		changeTextView(mTextView, spantext);
		textView2.setVisibility(View.INVISIBLE);
		showTimeBarBlockOnly();
		selectionStart = -1;
		selectionEnd = -1;
	}

	@Click(R.id.translateButton)
	public void translateClicked() {
		Util.openWordInGoogleTranslate(this, mTextView.getText().toString().substring(selectionStart, selectionEnd));
	}

	@Click(R.id.addButton)
	public void addClicked() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		View view = LayoutInflater.from(this).inflate(R.layout.dialog_write_translation, null);
		final EditText originalTextField = (EditText) view.findViewById(R.id.originalText);
		final EditText translationField = (EditText) view.findViewById(R.id.translationEditText);
		String originalText = mTextView.getText().toString().substring(selectionStart, selectionEnd);
		originalTextField.setText(originalText);
		translationField.requestFocus();
		ImageView pasteButton = (ImageView) view.findViewById(R.id.pasteButton);
		pasteButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ClipboardManager clipBoard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				String pastedText = clipBoard.getText().toString();
				translationField.setText(pastedText);
			}
		});
		builder.setView(view);
		builder.setPositiveButton(getString(R.string.dialog_save_translation), new Dialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int subnumber = subtitleManager.getCurrentSubtitleNumber();
				TranslationUnit unit = new TranslationUnit(subtitleManager.getCurrentSubtitleNumber(), selectionStart, selectionEnd,
						translationField.getText().toString());
				if (app.dataSource.addTranslation(subtitleManager.getSubtitleFile(), unit)) {
					RangeMap<Integer, TranslationUnit> map;
					if (translations.containsKey(subnumber)) {
						map = translations.get(subnumber);
					} else {
						map = TreeRangeMap.create();
					}
					map.put(Range.closed(unit.getStartIndex(), unit.getEndIndex()), unit);
					translations.put(subnumber, map);
				}
				clearClicked();
			}
		});
		builder.setNegativeButton(getString(R.string.dialog_cancel), null);
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putLong(SAVED_STATE_PLAY_FROM, mVideoView.getCurrentPosition());
		outState.putBoolean(SAVED_STATE_START, mVideoView.isPlaying());
	}

	@Click(R.id.imageView1)
	void bookClicked() {
		Intent intent = new Intent(this, SavedPhrasesActivity_.class);
		intent.putExtra(SavedPhrasesActivity.EXTRA_VIDEO_FILE, videoFilePath);
		intent.putExtra(SavedPhrasesActivity.EXTRA_SUBTITLES_FILE, subtitleManager.getSubtitleFile().getAbsolutePath());
		startActivity(intent);
	}

	@Click(R.id.editTranslationButton)
	void editTranslationClicked() {
		int subnumber = subtitleManager.getCurrentSubtitleNumber();
		final TranslationUnit targetUnitToEdit = translations.get(subnumber).getEntry(savedPhraseClicked).getValue();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		View view = LayoutInflater.from(this).inflate(R.layout.dialog_write_translation, null);
		final EditText originalTextField = (EditText) view.findViewById(R.id.originalText);
		final EditText translationField = (EditText) view.findViewById(R.id.translationEditText);
		String originalText = subtitleManager.getSubtitles().getItemAt(subnumber).getText();
		originalTextField.setText(originalText.substring(targetUnitToEdit.getStartIndex(), targetUnitToEdit.getEndIndex()));
		translationField.setText(targetUnitToEdit.getTranslation());
		translationField.requestFocus();
		ImageView pasteButton = (ImageView) view.findViewById(R.id.pasteButton);
		pasteButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ClipboardManager clipBoard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				String pastedText = clipBoard.getText().toString();
				translationField.setText(pastedText);
			}
		});
		builder.setView(view);
		builder.setPositiveButton(getString(R.string.dialog_update_translation), new Dialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				targetUnitToEdit.setTranslation(translationField.getText().toString());
				app.dataSource.updateTranslation(subtitleManager.getSubtitleFile(), targetUnitToEdit);
				clearClicked();
			}
		});
		builder.setNegativeButton(getString(R.string.dialog_cancel), new Dialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				clearClicked();
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
		savedPhraseClicked = -1;
	}

	@Click(R.id.removeTranslationButton)
	void removeTranslationClicked() {
		if (this.translations.containsKey(subtitleManager.getCurrentSubtitleNumber())) {
			int subnumber = subtitleManager.getCurrentSubtitleNumber();
			Range<Integer> targetRangeToDelete = translations.get(subnumber).getEntry(savedPhraseClicked).getKey();
			TranslationUnit targetUnitToDelete = translations.get(subnumber).getEntry(savedPhraseClicked).getValue();
			translations.get(subnumber).remove(targetRangeToDelete);
			app.dataSource.remove(subtitleManager.getSubtitleFile().getAbsolutePath(), targetUnitToDelete);
			Spannable spantext = new SpannableStringBuilder(mTextView.getText().toString());
			if (translations.containsKey(subnumber)) {
				for (TranslationUnit unit : translations.get(subnumber).asMapOfRanges().values()) {
					spantext.setSpan(new ForegroundColorSpan(Color.WHITE), unit.getStartIndex(), unit.getEndIndex(),
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
					spantext.setSpan(new BackgroundColorSpan(Color.BLUE), unit.getStartIndex(), unit.getEndIndex(),
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
			}
			changeTextView(mTextView, spantext);
			textView2.setVisibility(View.INVISIBLE);
			showTimeBarBlockOnly();
			selectionStart = -1;
			selectionEnd = -1;
			savedPhraseClicked = -1;
		}
	}

	private void showContextualBlockOnly() {
		timeBarBlock.setVisibility(View.GONE);
		contextualBlock.setVisibility(View.VISIBLE);
		editTranslationBlock.setVisibility(View.GONE);
	}

	private void showTimeBarBlockOnly() {
		timeBarBlock.setVisibility(View.VISIBLE);
		contextualBlock.setVisibility(View.GONE);
		editTranslationBlock.setVisibility(View.GONE);
	}

	private void showEditBlockOnly() {
		timeBarBlock.setVisibility(View.GONE);
		contextualBlock.setVisibility(View.GONE);
		editTranslationBlock.setVisibility(View.VISIBLE);
	}

	private void hideBlocks() {
		timeBarBlock.setVisibility(View.GONE);
		contextualBlock.setVisibility(View.GONE);
		editTranslationBlock.setVisibility(View.GONE);
	}

}