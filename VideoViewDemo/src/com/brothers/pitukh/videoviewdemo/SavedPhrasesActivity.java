package com.brothers.pitukh.videoviewdemo;

import java.io.IOException;

import org.holoeverywhere.app.AlertDialog;
import org.holoeverywhere.app.Dialog;
import org.holoeverywhere.widget.EditText;
import org.holoeverywhere.widget.Toast;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.ClipboardManager;
import android.util.Log;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.ContextMenu;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.brothers.pitukh.videoviewdemo.model.TranslationUnit;
import com.brothers.pitukh.videoviewdemo.subtitles.Subtitles.SubItem;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.App;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.Extra;
import com.googlecode.androidannotations.annotations.ItemClick;

@EActivity(R.layout.activity_listview_saved_phrases)
public class SavedPhrasesActivity extends org.holoeverywhere.app.ListActivity {

	@Extra(EXTRA_SUBTITLES_FILE)
	protected String subfile;
	@Extra(EXTRA_VIDEO_FILE)
	protected String videofile;

	@App
	Ap app;

	public static final String EXTRA_VIDEO_FILE = "ExtraVideoFile";
	public static final String EXTRA_SUBTITLES_FILE = "ExtraSubtitlesFile";
	private SavedPhrasesAdapter adapter;

	@AfterViews
	public void after() {
		getSupportActionBar().setSubtitle(videofile);
		
		Log.v(Ap.TAG, subfile);
		if (subfile == null) {
			Toast.makeText(this, "You didn't save phrases for this file yet", Toast.LENGTH_LONG).show();
			return;
		}
		try {
			adapter = new SavedPhrasesAdapter(this, subfile);
			setListAdapter(adapter);
		} catch (IOException e) {
			Log.e(Ap.TAG, "Message", e);
		} catch (Exception e) {
			Log.e(Ap.TAG, "Message2", e);
			Toast.makeText(this, "Sorry, some error occured :(", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
		menu.clear();
		getSupportMenuInflater().inflate(R.menu.saved_phrases_context, menu);
	}

	private int position;

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.item_play:
			Intent intent = new Intent(this, VideoPlayerActivity_.class);
			intent.putExtra(VideoPlayerActivity.EXTRA_VIDEOPLAYER_FILE_PATH, videofile);
			intent.putExtra(VideoPlayerActivity.EXTRA_VIDEOPLAYER_START, false);
			TranslationUnit clickedUnit = adapter.getItem(position);
			long startDate = adapter.getSubtitles().getItemAt(clickedUnit.getSubtitleNumber()).getStartDate().getTime();
			intent.putExtra(VideoPlayerActivity.EXTRA_VIDEOPLAYER_START_FROM, startDate);
			startActivity(intent);
			return true;
		case R.id.item_edit:
			edit();
			return true;
		case R.id.item_delete:
			adapter.remove(position);
			adapter.notifyDataSetChanged();
			return true;
		default:
			Log.e(Ap.TAG, "Unreachable part of code invoked");
			return false;
		}
	}

	@ItemClick(android.R.id.list)
	protected void itemClicked(int position) {
		this.position = position;
		startActionMode(new AnActionModeOfEpicProportions());
	}

	private final class AnActionModeOfEpicProportions implements ActionMode.Callback {

		private static final int playId = 0;
		private static final int editId = 1;
		private static final int deleteId = 2;

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuItem itemPlay = menu.add(Menu.NONE, playId, 0, getString(R.string.saved_phrase_play));
			MenuItem itemEdit = menu.add(Menu.NONE, editId, 1, getString(R.string.saved_phrase_edit));
			MenuItem itemDelete = menu.add(Menu.NONE, deleteId, 2, getString(R.string.saved_phrase_delete));
			itemPlay.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			itemEdit.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			itemDelete.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_WITH_TEXT);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case playId:
				Intent intent = new Intent(SavedPhrasesActivity.this, VideoPlayerActivity_.class);
				intent.putExtra(VideoPlayerActivity.EXTRA_VIDEOPLAYER_FILE_PATH, videofile);
				intent.putExtra(VideoPlayerActivity.EXTRA_VIDEOPLAYER_START, false);
				TranslationUnit clickedUnit = adapter.getItem(position);
				long startDate = adapter.getSubtitles().getItemAt(clickedUnit.getSubtitleNumber()).getStartDate().getTime();
				intent.putExtra(VideoPlayerActivity.EXTRA_VIDEOPLAYER_START_FROM, startDate);
				startActivity(intent);
				break;
			case editId:
				edit();
				break;
			case deleteId:
				adapter.remove(position);
				adapter.notifyDataSetChanged();
				break;
			default:
				Log.e(Ap.TAG, "Unreachable part of code invoked");
				return false;
			}
			mode.finish();
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
		}
	}

	private void edit() {
		final TranslationUnit clickedUnit = adapter.getItem(position);
		SubItem item = adapter.getSubtitles().getItemAt(clickedUnit.getSubtitleNumber());
		String originalText = item.getText().substring(clickedUnit.getStartIndex(), clickedUnit.getEndIndex());

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		View view = LayoutInflater.from(this).inflate(R.layout.dialog_write_translation, null);
		final EditText originalTextField = (EditText) view.findViewById(R.id.originalText);
		final EditText translationField = (EditText) view.findViewById(R.id.translationEditText);
		originalTextField.setText(originalText);
		translationField.setText(clickedUnit.getTranslation());
		translationField.requestFocus();
		ImageView pasteButton = (ImageView) view.findViewById(R.id.pasteButton);
		pasteButton.setOnClickListener(new android.view.View.OnClickListener() {
			@Override
			public void onClick(View v) {
				ClipboardManager clipBoard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
				String pastedText = clipBoard.getText().toString();
				translationField.setText(pastedText);
			}
		});
		builder.setView(view);
		builder.setPositiveButton(getString(R.string.dialog_update_translation), new Dialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				clickedUnit.setTranslation(translationField.getText().toString());
				app.dataSource.updateTranslation(subfile, clickedUnit);
				adapter.notifyDataSetChanged();
			}
		});
		builder.setNegativeButton(getString(R.string.dialog_cancel), null);
		AlertDialog dialog = builder.create();
		dialog.show();
	}

}
