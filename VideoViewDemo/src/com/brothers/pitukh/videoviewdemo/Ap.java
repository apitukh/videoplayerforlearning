package com.brothers.pitukh.videoviewdemo;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;

import com.brothers.pitukh.videoviewdemo.db.WordsDataSource;
import com.googlecode.androidannotations.annotations.EApplication;
import com.nostra13.universalimageloader.cache.disc.impl.FileCountLimitedDiscCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.ImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

@EApplication
public class Ap extends org.holoeverywhere.app.Application {
	
	static Ap instance;
	public static final String TAG = "LiveLanguage";
	WordsDataSource dataSource = new WordsDataSource(this);

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheOnDisc(true).displayer(new FadeInBitmapDisplayer(200))
				.build();
		ImageLoaderConfiguration.Builder builder = new ImageLoaderConfiguration.Builder(getApplicationContext());
		builder.enableLogging();
		builder.discCache(new FileCountLimitedDiscCache(StorageUtils.getIndividualCacheDirectory(this), 100));
		builder.defaultDisplayImageOptions(defaultOptions);
		builder.imageDownloader(new ImageDownloader() {
			protected static final int BUFFER_SIZE = 32 * 1024; // 32 Kb

			@Override
			public InputStream getStream(String videoUri, Object extra) throws IOException {
				Log.v(TAG, videoUri);
				if (Scheme.ofUri(videoUri) == Scheme.FILE) {
					String filePath = Scheme.FILE.crop(videoUri);
					return new BufferedInputStream(new FileInputStream(filePath), BUFFER_SIZE);
				}
				Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(videoUri, MediaStore.Video.Thumbnails.MINI_KIND);
				if (thumbnail == null) {
					return null;
				}
				thumbnail = Bitmap.createScaledBitmap(thumbnail, 512, 384, false);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				thumbnail.compress(CompressFormat.PNG, 0 /* ignored for PNG */, bos);
				byte[] bitmapdata = bos.toByteArray();
				ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
				return bs;
			}
		});
		ImageLoaderConfiguration config = builder.build();
		ImageLoader.getInstance().init(config);
	}

	/**
	 * Called when the overall system is running low on memory
	 */
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		Log.w("TAG", "System is running low on memory");
		BitmapCache.getInstance().clear();
	}

}
